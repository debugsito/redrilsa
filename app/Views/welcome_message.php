<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.0/main.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="stylesheet" href="/style.css">
</head>
<body>
<header class="header">
    <nav class="navbar navbar-light navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="/img/redrilsa.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto align-items-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Reporte de Operatividad Diaria
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Estado Maquinas</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link " href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="/img/user.jpeg" width="30" height="30" class="rounded-circle">
                            <span>Elias Farias</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<div class="container-etiqueta container">
    <p class="first-text-etiqueta">Cliente: CIA Buena Ventura</p>
    <p>OBRA/PROYECTO: Orcopampa</p>
    <p>CIUDAD: Arequipa</p>
    <div class="clear"></div>
</div>
<div class="container-reporte container">
    <h1 class="title">Reporte de Operatividad Diaria</h1>
    <div class="btn-new-report">
        <button type="button" class="btn btn-danger px-3">Nuevo</button>
        <div class="clear"></div>
    </div>
</div>
<div class="container">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Ocurrencia</th>
                <th scope="col">Motivo de la Parada</th>
                <th scope="col">Horometro</th>
                <th scope="col">Fecha Ingreso o <br> Parada Unidad</th>
                <th scope="col">Fecha Salida <br>o Termino</th>
                <th scope="col" class="text-center">Duración <br>del Evento</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Se realiza mantenimiento de sensor de guarda de seguridad.</td>
                <td>MC</td>
                <td>920</td>
                <td>01/10/2020</td>
                <td>01/10/2020</td>
                <td class="text-center">3</td>
            </tr>
            <tr>
                <td>Se realiza mantenimiento programado de 250 Hrs.</td>
                <td>MP</td>
                <td>760</td>
                <td>02/10/2020</td>
                <td>02/10/2020</td>
                <td class="text-center">3</td>
            </tr>
            <tr>
                <td>Se instala punto a tierra en nueva cabina de perforación</td>
                <td>INSTALACIÓN</td>
                <td>765</td>
                <td>03/10/2020</td>
                <td>03/10/2020</td>
                <td class="text-center">6</td>
            </tr>
            <tr>
                <td>Se realiza mantenimiento programado de 250 Hrs.</td>
                <td>MP</td>
                <td>2995</td>
                <td>05/10/2020</td>
                <td>05/10/2020</td>
                <td class="text-center">3</td>
            </tr>
            <tr>
                <td>Se realiza instalación hidráulica y electrica de equipo</td>
                <td>INSTALACIÓN</td>
                <td>765</td>
                <td>05/10/2020</td>
                <td>05/10/2020</td>
                <td class="text-center">3</td>
            </tr>
            <tr>
                <td>Se realiza activación de puesta a tierra.</td>
                <td>INSTALACIÓN</td>
                <td>963</td>
                <td>05/10/2020</td>
                <td>05/10/2020</td>
                <td class="text-center">3</td>
            </tr>
            <tr>
                <td>Se colocan pernos en tapa de cabezal chuck, se coloca guiadores a Rod holder y Unidad de Rotación y repara block de sincroninación del Rod Holder.</td>
                <td>MC</td>
                <td>781</td>
                <td>06/10/2020</td>
                <td>06/10/2020</td>
                <td class="text-center">3</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<nav aria-label="Page navigation example" class="container container-pagination-report">
    <ul class="pagination">
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">4</a></li>
        <li class="page-item"><a class="page-link" href="#">5</a></li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
