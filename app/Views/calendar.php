<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.0/main.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="stylesheet" href="/style.css">
</head>
<body>

<header class="header">
    <nav class="navbar navbar-light navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="/img/redrilsa.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto align-items-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Reporte de Operatividad Diaria
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Estado Maquinas</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link " href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="/img/user.jpeg" width="30" height="30" class="rounded-circle">
                            <span>Elias Farias</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<div class="container-etiqueta container">
    <p class="first-text-etiqueta">Cliente: CIA Buena Ventura</p>
    <p>OBRA/PROYECTO: Orcopampa</p>
    <p>CIUDAD: Arequipa</p>
</div>
<div class="container">
    <h1 class="title">Reporte de Operatividad Diaria</h1>
    <div class="btn-new-report">
        <div class="clear"></div>
    </div>
</div>
<div class="container">
    <div id='calendar'></div>
</div>
<br><br>
<!-- Modal -->
<div class="modal fade" id="modalEvent" tabindex="-1" role="dialog" aria-labelledby="modalEvent" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Equipos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <div class="font-weight-bold">LM75-546</div>
                    <div>
                        <label class="customCheck">Operativo
                            <input type="radio" checked="checked" name="radio">
                            <span class="checkmark green"></span>
                        </label>
                    </div>
                    <div>
                        <label class="customCheck">Otro
                            <input type="radio" name="radio">
                            <span class="checkmark green"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.0/main.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.0/locales-all.min.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {
            timeZone: 'UTC',
            locale: 'es',
            firstDay:0,
            themeSystem: 'bootstrap',
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth'
            },
            weekNumbers: true,
            dayMaxEvents: true,
            events: [
                {
                    title  : 'LM75-31',
                    start  : '2021-01-01',
                    eventClassNames:'green',
                    groupEvent:'O'
                },
                {
                    title  : 'RD 800 - 13M',
                    start  : '2021-01-05',
                    eventClassNames:'yellow',
                    groupEvent:'MC'
                },
                {
                    title  : 'RD 800 - 13M',
                    start  : '2021-01-06',
                    eventClassNames:'orange',
                    groupEvent:'MP'
                },
            ],
            eventContent: function (info) {
                return {
                    html: '<span class="typeEvent '+info.event.extendedProps.eventClassNames+'">'+info.event.extendedProps.groupEvent+'</span>'+ '<span>'+info.event.title+'</span>'
                }
            },
            eventClick: function(info) {

                $("#modalEvent").modal("show");
                /*alert('Event: ' + info.event.title);
                alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
                alert('View: ' + info.view.type);

                // change the border color just for fun
                info.el.style.borderColor = 'red';*/
            },
            eventDidMount: function(info) {console.log(info);
            }
        });

        calendar.render();
    });
</script>
</body>
</html>
